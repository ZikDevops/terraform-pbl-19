region = "us-east-1"

vpc_cidr = "10.0.0.0/16"

enable_dns_support = "true"

enable_dns_hostnames = "true"

# enable_classiclink = "false"

# enable_classiclink_dns_support = "false"

preferred_number_of_public_subnets = 2

preferred_number_of_private_subnets = 4

environment = "dev"

ami-web = "ami-08cdcd865e1c5ddb0"

ami-bastion = "ami-0ff6a87d9ef398315"

ami-nginx = "ami-08fc2b66cbb758e00"

ami-sonar = "ami-0570824b2f84e14f5"

keypair = "Isaac"

master-password = "devopspblproject"

master-username = "isaac"

account_no = "372759426427"

tags = {
  Owner-Email     = "izikayanda@gmail.com"
  Managed-By      = "Terraform"
  Billing-Account = "1234567890"
}
